export function validatePassword(password) {
    let message = "";
    if(!password.match(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\.$*&@#!])(?:([0-9a-zA-Z\.$*&@#!])(?!\1)){6,}$/)) {
        message = 'A senha deve conter pelo menos 8 caracteres, incluindo um número, uma letra maiúscula, uma letra minúscula e um caractere especial.';
    }
    return { message };
  }
  