import bcrypt from 'bcrypt';
const saltRounds = 15;

export function encryptPassword(password) {
  return bcrypt.hashSync(password, saltRounds);
}