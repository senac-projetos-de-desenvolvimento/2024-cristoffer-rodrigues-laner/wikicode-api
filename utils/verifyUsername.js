export function validateUsername(username) {
    let message = "";

    if(username.match(/[\u00C0-\u017F \/\.\=\-\,!@#$%¨&*()\+\-]/)) {
        message = 'O usuário deve conter apenas letras e números.';
    }
    return { message };
}