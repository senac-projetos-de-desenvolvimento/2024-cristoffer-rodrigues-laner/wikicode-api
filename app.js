import express from 'express';
import cors from 'cors';
import { sequelize } from './databases/connectDb.js'; 
import { User } from './models/Users.js';
import { Publication } from './models/Publications.js';
import { Participant } from './models/Participants.js';

import router from './routes/index.js';
import { Category } from './models/Category.js';
import { Review } from './models/Reviews.js';
import { Client } from './models/Clients.js';

const app = express()
let port = process.env.PORT || 6969

app.use(express.json())
app.use(cors())
app.use(router)

app.use("/api", router)

app.get("/", (req, res) => {
    res.send('Hello World')
})

async function connectDb() {
    try {
        // syncs
        await sequelize.authenticate()
        await Client.sync({ alter: true })
        await Category.sync({ alter: true })
        await User.sync({ alter: true })
        await Publication.sync({ alter: true })
        await Participant.sync({ alter: true })
        await Review.sync({ alter: true })
        console.log('Conection has been established successfully.')
    } catch (error) {
        console.error('Unable to connect to the database:', error)
    }
}

connectDb();

app.listen(port, () => {
    console.log(`Server listening at http://localhost:${port}`)
});