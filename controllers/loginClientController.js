import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'
import * as dotenv from 'dotenv'
dotenv.config()

import { Client } from '../models/Clients.js'

export const loginClient = async (req, res) => {
  const { email, password } = req.body
  const defaultError = "Erro... e-mail ou senha inválido."

  if (!email || !password) {
    res.status(401).json({msg: "Informe e-mail e senha."})
    return
  }

  try {
    const client = await Client.findOne({ where: { email } })

    if (client == null) {
      res.status(401).json({ erro: defaultError})
      return
    }

    if (bcrypt.compareSync(password, client.password)) {
      const token = jwt.sign({
        client_logado_id: client.id,
        client_logado_nome: client.name,
        client_logado_email: client.email
      },
        process.env.JWT_SECRET_KEY,
        { expiresIn: "1d" }
      )

      res.status(200).json({name: client.name, id: client.id, email: client.email, token})
    } else {     
      res.status(401).json({ erro: defaultError})
    }
  } catch (error) {
    res.status(401).json(error)
  }
}