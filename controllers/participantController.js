import { Participant } from "../models/Participants.js";
import { Publication } from "../models/Publications.js";
import { User } from "../models/Users.js";

// obsoleto por enquanto
export const participantIndex = async (req, res) => {
    const { id } = req.params;

    try {
        const participants = await Participant.findAll({
            include: [
                {
                  model: User,
                  attributes: ['name']
                },
                {
                    model: Publication,
                    attributes: ['title']
                }
              ]
        }, {
            where: { publication_id: id }
        })
        res.status(200).json(participants)
    } catch (error) {
        console.error(`error when searching for participants ${new Date()}: ${error}`);
        res.status(400).json({ error: error.message });
    }
}

export const participantDelete = async (req, res) => {
    // const permission = req.user_logado_permissao
    const { id } = req.params;

    // if(permission != "Editor" || permission != "Master") {
    //     res.status(400).json({msg: "Sem autorização..."})
    //     return
    // }

    // if(existingPublication === null) {
    //     res.status(404).json({ error: 'Publication ID not found.' });
    //     return;
    // }

    try {
        await Participant.destroy({
            where: { publicationId: id }
        })
        res.status(201).json({ message: `Participants deleted successfully!` });
    } catch (error) {
        console.error(`error when deleting participants ${new Date()}: ${error}.`)
        res.status(400).json({ error: error.message })
    }
}