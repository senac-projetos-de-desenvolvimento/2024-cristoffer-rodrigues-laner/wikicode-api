import { User } from "../models/Users.js"
import { validatePassword } from '../utils/verifyPassword.js';
import { encryptPassword } from '../utils/encryptedPassword.js';
import { validateUsername } from "../utils/verifyUsername.js";

export const userIndex = async (req, res) => {
    const permission = req.user_logado_permissao

    if(permission != "Master") {
        res.status(400).json({msg: "Sem autorização..."})
        return
    }

    try {
        const users = await User.findAll()
        res.status(200).json(users)
    } catch (error) {
        console.error(`error when searching for users ${new Date()}: ${error}`);
        res.status(400).json({ error: error.message });
    }
}

export const userIndexNames = async (req, res) => {
    try {
        const users = await User.findAll({
            attributes: ['id', 'name']
        })
        res.status(200).json(users)
    } catch (error) {
        console.error(`error when searching for users names ${new Date()}: ${error}`);
        res.status(400).json({ error: error.message });
    }
}

export const userAdminIndex = async (req, res) => {
    const permission = req.user_logado_permissao

    if(permission != "Master") {
        res.status(400).json({msg: "Sem autorização...r"})
        return
    }

    try {
        const users = await User.findAll()
        
        res.status(200).json(users)
    } catch (error) {
        console.error(`error when searching for users ${new Date()}: ${error}`);
        res.status(400).json({ error: error.message });
    }
}

export const userPesqId = async (req, res) => {
    const { id } = req.params
    const permission = req.user_logado_permissao

    if(permission != "Master") {
        res.status(400).json({msg: "Sem autorização..."})
        return
    }

    if(!id) {
        res.status(400).json({msg: "Informe o ID do usuário."})
        return
    }

    try {
        const user = await User.findByPk(id)
        res.status(200).json(user)
    } catch (error) {
        console.error(`error when searching for user ${new Date()}: ${error}`)
        res.status(400).json(error)
    }
}

export const userCreate = async (req, res) => {
    const { name, username, permission } = req.body
    let { password } = req.body
    const passValidate = validatePassword(password)
    const usernameValidate = validateUsername(username)
    const existingUser = await User.findOne({
        where: {
            username,
        },
    });

    if(!name || !username || !password || !permission) {
        res.status(400).json({ error: 'Missing name, username, password or permission.' });
        return;
    }
    
    if (passValidate.message.length > 0) {
        return res.status(400).json({error: passValidate.message});
    }

    if (usernameValidate.message.length > 0) {
        return res.status(400).json({error: usernameValidate.message});
    }

    if (existingUser) {
        return res.status(400).json({ error: 'Esse usuário já existe.' });
    }

    try {
        password = encryptPassword(password)
        const user = await User.create({
            name, username, password, permission
        })
        res.status(201).json({ message: 'User created successfully!', user });
    } catch (error) {
        console.error(`error when creating user ${new Date()}: ${error}.`)
        res.status(400).json({ error: error.message })
    }
}

export const userUpdate = async (req, res) => {
    const { id } = req.params;
    let { password } = req.body
    const { name, username, permission } = req.body
    const existingUser = await User.findByPk(id)
    const passValidate = validatePassword(password)
    const usernameValidate = validateUsername(username)
    const levelPermission = req.user_logado_permissao

    if(levelPermission != "Master") {
        res.status(400).json({msg: "Sem autorização..."})
        return
    }

    if(!id) {
        res.status(400).json({ error: 'Missing user ID.' });
        return;
    }

    if(existingUser === null) {
        res.status(404).json({ error: 'Publication ID not found.' });
        return;
    }

    if(!name || !username || !password || !permission) {
        res.status(400).json({ error: 'Missing name, username, password or permission.' });
        return;
    }

    if (passValidate.message.length > 0) {
        return res.status(400).json(passValidate.message);
    }

    if (usernameValidate.message.length > 0) {
        return res.status(400).json(usernameValidate.message);
    }

    try {
        password = encryptPassword(password)
        const user = await existingUser.update({
            name, username, password, permission
        });

        res.status(201).json({ message: `User '${user.username}' updated successfully!` });
    } catch (error) {
        console.error(`error when updating user ${new Date()}: ${error}.`)
        res.status(400).json({ error: error.message })
    }
}

// export const userPasswordUpdate = async (req, res) => {
//     const { id } = req.params;
//     const existingUser = await User.findByPk(id)
//     const { oldPassword, newPassword } = req.body
//     const passValidate = validatePassword(req.body.newPassword);

//     if(!id) {
//         res.status(400).json({ error: 'Missing user ID.' });
//         return;
//     }

//     if(existingUser === null) {
//         res.status(404).json({ error: 'User ID not found.' });
//         return;
//     }

//     if(!oldPassword || !newPassword) {
//         res.status(400).json({ error: 'Missing oldPassword or newPassword.' });
//         return;
//     }

//     if (passValidate.message.length > 0) {
//         return res.status(400).json(passValidate.message);
//     }

//     try {
//         const isMatch = await bcrypt.compare(oldPassword, existingUser.password);
//         if (!isMatch) {
//             return res.status(400).json({ error: 'Old password is incorrect.' });
//         }

//         const password = encryptPassword(newPassword);

//         await existingUser.update({
//             password: password,
//         });

//         res.status(200).json({ message: `Password for user '${existingUser.name}' successfully changed!` });
//     } catch (error) {
//         console.error(`error when updating user ${new Date()}: ${error}.`)
//         res.status(400).json({ error: error.message })
//     }
// }



export const userDelete = async (req, res) => {
    const { id } = req.params;
    const existingUser = await User.findByPk(id)
    const permission = req.user_logado_permissao

    if(permission != "Master") {
        res.status(400).json({ error: 'Sem autorização.' });
        return;
    }

    if(!id) {
        res.status(400).json({ error: 'Missing user ID.' });
        return;
    }

    if(existingUser === null) {
        res.status(404).json({ error: 'User ID not found.' });
        return;
    }

    try {
        await User.destroy({
            where: { id }
        })
        res.status(201).json({ message: `User '${existingUser.name}' deleted successfully!` });
    } catch (error) {
        console.error(`error when deleting user ${new Date()}: ${error}.`)
        res.status(400).json({ error: error.message })
    }
}