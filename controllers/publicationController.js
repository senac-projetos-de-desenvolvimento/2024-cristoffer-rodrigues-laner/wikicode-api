import { Category } from "../models/Category.js";
import { Publication } from "../models/Publications.js";
import { User } from "../models/Users.js";

export const publicationIndex = async (req, res) => {
    try {
        const publications = await Publication.findAll({
            order: [
                ['createdAt', 'DESC']
            ],
            include: [
                {
                  model: User,
                  attributes: ['name', 'permission'],
                },
                {
                    model: User,
                    as: 'Participants',
                    through: { attributes: [] }
                },
                {
                    model: Category,
                    attributes: ['id', 'name'],
                }
              ]
        })
        res.status(200).json(publications)
    } catch (error) {
        console.error(`error when searching for publications ${new Date()}: ${error}`);
        res.status(400).json({ error: error.message });
    }
}

export const publicationsPublics = async (req, res) => {
    try {
        const publications = await Publication.findAll({
            order: [
                ['createdAt', 'DESC']
            ],
            include: [
                {
                  model: User,
                  attributes: ['name', 'permission'],
                },
                {
                    model: User,
                    as: 'Participants',
                    through: { attributes: [] }
                },
                {
                    model: Category,
                    attributes: ['id', 'name'],
                }
            ],
            where: {
                isPublic: true
            }
        })
        res.status(200).json(publications)
    } catch (error) {
        console.error(`error when searching for publications ${new Date()}: ${error}`);
        res.status(400).json({ error: error.message });
    }
}

export const publicationAdminIndex = async (req, res) => {
    const permission = req.user_logado_permissao

    if(permission != "Editor" && permission != "Master") {
        res.status(400).json({msg: "Sem autorização... Leitor"})
        return
    }

    try {
        const publications = await Publication.findAll({
            order: [
                ['createdAt', 'DESC']
            ],
            include: [
                {
                  model: User,
                  attributes: ['name', 'permission'],
                },
                {
                    model: Category,
                    attributes: ['id', 'name']
                }
              ]
        })
        
        res.status(200).json(publications)
    } catch (error) {
        console.error(`error when searching for publications ${new Date()}: ${error}`);
        res.status(400).json({ error: error.message });
    }
}

export const publicationPesqId = async (req, res) => {
    const { id } = req.params

    try {
        const publications = await Publication.findByPk(id, {
            include: [
                {
                  model: User,
                  attributes: ['name', 'permission'],
                },
                {
                    model: User,
                    as: 'Participants',
                    through: { attributes: [] }
                },
                {
                    model: Category,
                    attributes: ['id', 'name'],
                  }
              ]
        })
        res.status(200).json(publications)
    } catch (error) {
        console.error(`error when searching for publications ${new Date()}: ${error}`);
        res.status(400).json({ error: error.message });
    }
}

export const publicationPublicPesqId = async (req, res) => {
    const { id } = req.params

    try {
        const publications = await Publication.findByPk(id, {
            include: [
                {
                  model: User,
                  attributes: ['name', 'permission'],
                },
                {
                    model: User,
                    as: 'Participants',
                    through: { attributes: [] }
                },
                {
                    model: Category,
                    attributes: ['id', 'name'],
                  }
              ],
              where: {
                isPublic: true
              }
        })
        res.status(200).json(publications)
    } catch (error) {
        console.error(`error when searching for publications ${new Date()}: ${error}`);
        res.status(400).json({ error: error.message });
    }
}

export const publicationPesqByCategory = async (req, res) => {
    const { category } = req.params

    if(!category) {
        res.status(400).json({ error: 'Missing category.' })
    }

    try {
        const publications = await Publication.findAll({
            order: [
                ['createdAt', 'DESC']
            ],
            where: { category },
            include: [
                {
                  model: User,
                  attributes: ['name', 'permission'],
                }
              ]
        })
        res.status(200).json(publications)
    } catch (error) {
        console.error(`error when searching category ${new Date()}: ${error}.`)
        res.status(400).json({ error: error.message })
    }
}

export const publicationCreate = async (req, res) => {
    const { participants, title, subtitle, description, isPublic, category_id, user_id } = req.body
    const permission = req.user_logado_permissao

    if(permission != "Editor" && permission != "Master") {
        res.status(400).json({msg: "Sem autorização... Leitor"})
        return
    }

    if(!title || !subtitle || !description || !isPublic || !category_id || !user_id) {
        res.status(400).json({ error: 'Missing title, subtitle, description, isPublic, category_id or user_id.' });
        return;
    }

    try {
        const publication = await Publication.create({
            title, subtitle, description, isPublic, category_id, user_id
        }).then((publication) => {
            if(participants && participants.length > 0) {
                publication.addParticipants(participants)
            }
        })
        res.status(201).json({ message: 'Publication created successfully!', publication });
    } catch (error) {
        console.error(`error when creating publication ${new Date()}: ${error}.`)
        res.status(400).json({ error: error.message })
    }
}

export const publicationUpdate = async (req, res) => {
    const { id } = req.params;
    const existingPublication = await Publication.findByPk(id)
    const { title, subtitle, description, isPublic, category_id, user_id } = req.body

    if(!id) {
        res.status(400).json({ error: 'Missing user ID.' });
        return;
    }

    if(existingPublication === null) {
        res.status(404).json({ error: 'Publication ID not found.' });
        return;
    }

    if(!title || !subtitle || !description || !isPublic || !category_id || !user_id) {
        res.status(400).json({ error: 'Missing title, subtitle, description, isPublic, category_id or user_id.' });
        return;
    }

    try {
        const publication = await existingPublication.update({
            title, subtitle, description, isPublic, category_id, user_id
        });

        res.status(201).json({ message: `Publication '${publication.title}' updated successfully!` });
    } catch (error) {
        console.error(`error when updating publication ${new Date()}: ${error}.`)
        res.status(400).json({ error: error.message })
    }
}

export const publicationDelete = async (req, res) => {
    const permission = req.user_logado_permissao
    const { id } = req.params;
    const existingPublication = await Publication.findByPk(id)

    if(permission != "Master") {
        res.status(400).json({msg: "Sem autorização... Editor"})
        return
    }

    if(!id) {
        res.status(400).json({ error: 'Missing user ID.' });
        return;
    }

    if(existingPublication === null) {
        res.status(404).json({ error: 'Publication ID not found.' });
        return;
    }

    try {
        await Publication.destroy({
            where: { id }
        })
        res.status(201).json({ message: `Publication '${existingPublication.title}' deleted successfully!` });
    } catch (error) {
        console.error(`error when deleting publication ${new Date()}: ${error}.`)
        res.status(400).json({ error: error.message })
    }
}