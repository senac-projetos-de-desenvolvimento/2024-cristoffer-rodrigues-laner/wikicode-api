import { validatePassword } from '../utils/verifyPassword.js';
import { encryptPassword } from '../utils/encryptedPassword.js';
import { validateUsername } from "../utils/verifyUsername.js";
import { Client } from "../models/Clients.js";

export const clientIndex = async (req, res) => {
    const permission = req.user_logado_permissao

    if(permission != "Master") {
        res.status(400).json({msg: "Sem autorização..."})
        return
    }

    try {
        const clients = await Client.findAll()
        res.status(200).json(clients)
    } catch (error) {
        console.error(`error when searching for clients ${new Date()}: ${error}`);
        res.status(400).json({ error: error.message });
    }
}

export const clientIndexNames = async (req, res) => {
    try {
        const clients = await Client.findAll({
            attributes: ['id', 'name']
        })
        res.status(200).json(clients)
    } catch (error) {
        console.error(`error when searching for clients names ${new Date()}: ${error}`);
        res.status(400).json({ error: error.message });
    }
}

export const clientPesqId = async (req, res) => {
    const { id } = req.params
    const permission = req.user_logado_permissao

    if(permission != "Master") {
        res.status(400).json({msg: "Sem autorização..."})
        return
    }

    if(!id) {
        res.status(400).json({msg: "Informe o ID do usuário."})
        return
    }

    try {
        const client = await Client.findByPk(id)
        res.status(200).json(client)
    } catch (error) {
        console.error(`error when searching for user ${new Date()}: ${error}`)
        res.status(400).json(error)
    }
}

export const clientCreate = async (req, res) => {
    const { name, email } = req.body
    let { password } = req.body
    const passValidate = validatePassword(password)
    const existingClient = await Client.findOne({
        where: {
            email,
        },
    });

    if(!name || !email || !password) {
        res.status(400).json({ error: 'Missing name, email or password.' });
        return;
    }
    
    if (passValidate.message.length > 0) {
        return res.status(400).json({error: passValidate.message});
    }

    if (existingClient) {
        return res.status(400).json({ error: 'Esse e-mail já possui cadastro.' });
    }

    try {
        password = encryptPassword(password)
        const client = await Client.create({
            name, email, password
        })
        res.status(201).json({ message: 'Client created successfully!', client });
    } catch (error) {
        console.error(`error when creating client ${new Date()}: ${error}.`)
        res.status(400).json({ error: error.message })
    }
}

export const clientUpdate = async (req, res) => {
    const { id } = req.params;
    let { password } = req.body
    const { name, email } = req.body
    const existingClient = await Client.findByPk(id)
    const passValidate = validatePassword(password)
    const usernameValidate = validateUsername(username)
    const levelPermission = req.user_logado_permissao

    if(levelPermission != "Master") {
        res.status(400).json({msg: "Sem autorização..."})
        return
    }

    if(!id) {
        res.status(400).json({ error: 'Missing client ID.' });
        return;
    }

    if(existingClient === null) {
        res.status(404).json({ error: 'Client ID not found.' });
        return;
    }

    if(!name || !email || !password) {
        res.status(400).json({ error: 'Missing name, email or password.' });
        return;
    }

    if (passValidate.message.length > 0) {
        return res.status(400).json(passValidate.message);
    }

    if (usernameValidate.message.length > 0) {
        return res.status(400).json(usernameValidate.message);
    }

    try {
        password = encryptPassword(password)
        const client = await existingClient.update({
            name, email, password
        });

        res.status(201).json({ message: `Client '${client.name}' updated successfully!` });
    } catch (error) {
        console.error(`error when updating client ${new Date()}: ${error}.`)
        res.status(400).json({ error: error.message })
    }
}

export const clientDelete = async (req, res) => {
    const { id } = req.params;
    const existingClient = await Client.findByPk(id)
    const permission = req.user_logado_permissao

    if(permission != "Master") {
        res.status(400).json({ error: 'Sem autorização.' });
        return;
    }

    if(!id) {
        res.status(400).json({ error: 'Missing user ID.' });
        return;
    }

    if(existingClient === null) {
        res.status(404).json({ error: 'Client ID not found.' });
        return;
    }

    try {
        await Client.destroy({
            where: { id }
        })
        res.status(201).json({ message: `Client '${existingClient.name}' deleted successfully!` });
    } catch (error) {
        console.error(`error when deleting client ${new Date()}: ${error}.`)
        res.status(400).json({ error: error.message })
    }
}