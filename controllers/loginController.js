import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'
import * as dotenv from 'dotenv'
dotenv.config()

import { User } from '../models/Users.js'

export const loginUser = async (req, res) => {
  const { username, password } = req.body
  const defaultError = "Erro... usuário ou senha inválido."

  if (!username || !password) {
    res.status(401).json({msg: "Informe usuário e senha."})
    return
  }

  try {
    const user = await User.findOne({ where: { username } })

    if (user == null) {
      res.status(401).json({ erro: defaultError})
      return
    }

    if (bcrypt.compareSync(password, user.password)) {
      const token = jwt.sign({
        user_logado_id: user.id,
        user_logado_nome: user.name,
        user_logado_permissao: user.permission
      },
        process.env.JWT_KEY,
        { expiresIn: "1d" }
      )

      res.status(200).json({name: user.name, id: user.id, permissao: user.permission , token})
    } else {     
      res.status(401).json({ erro: mensaErroPadrao})
    }
  } catch (error) {
    res.status(401).json(error)
  }
}