import { Category } from "../models/Category.js";

export const categoryIndex = async (req, res) => {
    try {
        const categories = await Category.findAll({
            order: ['name']
        })
        res.status(200).json(categories)
    } catch (error) {
        console.error(`error when searching for categories ${new Date()}: ${error}`);
        res.status(400).json({ error: error.message });
    }
}

export const categoryPesqId = async (req, res) => {
    const { id } = req.params

    try {
        const category = await Category.findByPk(id)
        res.status(200).json(category)
    } catch (error) {
        console.error(`error when searching for category ${new Date()}: ${error}`);
        res.status(400).json({ error: error.message });
    }
}

export const categoryCreate = async (req, res) => {
    const permission = req.user_logado_permissao
    const { name } = req.body
    const existingCategoryName = await Category.findOne({
        where: {
            name,
        },
    });

    if(existingCategoryName) {
        res.status(400).json({error: "Esse nome de categoria já existe."})
        return
    }

    if(permission != "Editor" && permission != "Master") {
        res.status(400).json({error: "Sem autorização..."})
        return
    }

    if(!name) {
        res.status(400).json({ error: 'Informe uma categoria.' });
        return;
    }

    try {
        const category = await Category.create({
            name
        })
        res.status(201).json({ message: 'Category created successfully!', category, status: 201 });
    } catch (error) {
        console.error(`error when creating category ${new Date()}: ${error}.`)
        res.status(400).json({ error: error.message })
    }
}

export const categoryUpdate = async (req, res) => {
    const permission = req.user_logado_permissao
    const { id } = req.params
    const { name } = req.body
    const existingCategory = await Category.findByPk(id)

    if(permission != "Editor" && permission != "Master") {
        res.status(400).json({msg: "Sem autorização..."})
        return
    }

    if(existingCategory === null) {
        res.status(404).json({ error: 'Category ID not found.' });
        return;
    }

    try {
        await Category.update({
            name
        }, {
            where: { id }
        })
        res.status(201).json({ message: `Category updated successfully!` });
    } catch (error) {
        console.error(`error when updating category ${new Date()}: ${error}.`)
        res.status(400).json({ error: error.message })
    }
}

export const categoryDelete = async (req, res) => {
    const permission = req.user_logado_permissao
    const { id } = req.params
    const existingCategory = await Category.findByPk(id)

    if(permission != "Editor" && permission != "Master") {
        res.status(400).json({msg: "Sem autorização..."})
        return
    }

    if(existingCategory === null) {
        res.status(404).json({ error: 'Category ID not found.' });
        return;
    }

    try {
        await Category.destroy({
            where: { id }
        })
        res.status(201).json({ message: `Category deleted successfully!` });
    } catch (error) {
        console.error(`error when deleting category ${new Date()}: ${error}.`)
        res.status(400).json({ error: error.message })
    }
}