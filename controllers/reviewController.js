import { Op } from "sequelize"
import { sequelize } from '../databases/connectDb.js'
import { Review } from '../models/Reviews.js';
import { User } from '../models/Users.js';
import { Publication } from '../models/Publications.js';
import { Client } from "../models/Clients.js";

export const reviewIndex = async (req, res) => {
  try {
    const reviews = await Review.findAll({
      include: [
        {
          model: User,
          attributes: ['name']
        },
        {
          model: Publication,
          attributes: ['id', 'title', 'subtitle', 'isPublic', 'sum', 'num']
        } 
      ],
      order: [['id', 'desc']]
    });
    res.status(200).json(reviews)
  } catch (error) {
    res.status(400).send(error)
  }
}

export const reviewPublication = async (req, res) => {
  const { publication_id } = req.params

  try {
    const reviews = await Review.findAll({
      where: { publication_id },
      include: [
        {
          model: User,
          attributes: ['name', 'permission'],
        },
        {
          model: Client,
          attributes: ['name'],
        }
      ],
      order: [['id', 'asc']]
    });    
    res.status(200).json(reviews)
  } catch (error) {
    res.status(400).send(error)
  }
}

export const reviewGraphStars = async (req, res) => {

  try {
    const reviews = await Review.findAll({
      attributes: ['stars',
        [sequelize.fn('count', sequelize.col('id')), 'num']],
      group: 'stars'
    });
    res.status(200).json(reviews)
  } catch (error) {
    res.status(400).send(error)
  }
}

export const reviewGraphDays = async (req, res) => {

  const data = new Date()           // obtém a data atual
  data.setDate(data.getDate() - 7)  // subtrai 7 dias 

  const dia = data.getDate().toString().padStart(2, "0")
  const mes = (data.getMonth() + 1).toString().padStart(2, "0")
  const ano = data.getFullYear()

  const atras_7 = ano + "-" + mes + "-" + dia

  try {
    const reviews = await Review.findAll({
      attributes: [
        [sequelize.fn('DAY', sequelize.col('data')), "dia"],
        [sequelize.fn('MONTH', sequelize.col('data')), "mes"],
        'stars',
        [sequelize.fn('count', sequelize.col('id')), 'num']],
      group: [
        sequelize.fn('DAY', sequelize.col('data')),
        sequelize.fn('MONTH', sequelize.col('data')),
        'stars'],
      where: { date: { [Op.gte]: atras_7 } }
    });
    res.status(200).json(reviews)
  } catch (error) {
    res.status(400).send(error)
  }
}

export const reviewCreate = async (req, res) => {
  const { publication_id, user_id, client_id, comment, stars } = req.body
  // se não informou estes atributos
  if (!publication_id || !comment || !stars) {
    res.status(400).json({ id: 0, msg: "Erro... Informe os dados" })
    return
  }

  const t = await sequelize.transaction();

  try {

    const review = await Review.create({
        publication_id, user_id, client_id, comment, stars, date: new Date()
    }, { transaction: t });

    await Publication.increment('sum',
      { by: stars, where: { id: publication_id }, transaction: t }
    );

    await Publication.increment('num',
      { by: 1, where: { id: publication_id }, transaction: t }
    );

    await t.commit();
    res.status(201).json(review)

  } catch (error) {

    await t.rollback();
    res.status(400).json({ "id": 0, "Erro": error })

  }
}

export const reviewDestroy = async (req, res) => {
  const { id } = req.params

  const t = await sequelize.transaction();

  try {

    const review = await Review.findByPk(id)

    await Publication.decrement('sum',
      {
        by: review.stars,
        where: { id: review.publication_id },
        transaction: t
      }
    );

    await Publication.decrement('num',
      {
        by: 1,
        where: { id: review.publication_id },
        transaction: t
      }
    );

    await Review.destroy({
      where: { id }
    });

    await t.commit();
    res.status(200).json({ msg: "Ok! Avaliação Excluída com Sucesso" })

  } catch (error) {

    await t.rollback();
    res.status(400).json({ "id": 0, "Erro": error })

  }
}

export const dadosGerais = async (req, res) => {

  // new Date().toISOString() retorna "2023-11-21T21:12:05"
  // com o split, separamos pelo "T" e pegamos só a 1ª parte
  const dataAtual = new Date().toISOString().split("T")[0]

  try {
    const clientes = await User.count()
    const publications = await Publication.count()
    const media = await Publication.findOne({
      attributes: [[sequelize.fn('avg', sequelize.col('preco')), 'preco']]
    })
    const avaliacoes = await Review.count()
    const avaliacoes_dia = await Review.count({
      where: { data: { [Op.gte]: dataAtual } }
    })
    //    res.status(200).json({ clientes, filmes, media: media ? media.preco : 0, avaliacoes, avaliacoes_dia })
    res.status(200).json({ clientes, publications, media, avaliacoes, avaliacoes_dia })
  } catch (error) {
    res.status(400).send(error)
  }
}