import { sequelize } from "../databases/connectDb.js";
import { DataTypes } from "sequelize";
import { User } from "./Users.js";
import { Participant } from "./Participants.js";
import { Category } from "./Category.js";

export const Publication = sequelize.define('publication', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    title: {
        type: DataTypes.STRING(60),
        allowNull: false
    },
    subtitle: {
        type: DataTypes.STRING(100),
        allowNull: false
    },
    description: {
        type: DataTypes.TEXT,
        allowNull: false
    },
    isPublic: {
        type: DataTypes.BOOLEAN,
        allowNull: false
    },
    sum: {
        type: DataTypes.INTEGER,
        defaultValue: 0
    },
    num: {
        type: DataTypes.INTEGER,
        defaultValue: 0
    },
}, {
    tableName: 'publications',
})

Publication.belongsTo(User, {
    foreignKey: {
        name: "user_id",
        allowNull: false
    },
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE'
})

User.hasMany(Publication, {
    foreignKey: {
        name: "user_id"
    }
})

Publication.belongsToMany(User, { through: Participant, as: 'Participants'});
User.belongsToMany(Publication, { through: Participant});

Publication.belongsTo(Category, {
    foreignKey: {
        name: "category_id",
        allowNull: false
    },
    onDelete: 'RESTRICT',
    onUpdate: 'CASCADE',
})

Category.hasMany(Publication, {
    foreignKey: {
        name: "category_id"
    }
})