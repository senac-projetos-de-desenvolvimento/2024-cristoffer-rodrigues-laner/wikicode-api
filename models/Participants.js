import { sequelize } from "../databases/connectDb.js";
import { DataTypes } from "sequelize";

export const Participant = sequelize.define('participant', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    }
}, {
    tableName: 'participants',
})

