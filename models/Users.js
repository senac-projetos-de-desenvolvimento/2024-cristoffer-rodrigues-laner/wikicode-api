import { sequelize } from "../databases/connectDb.js";
import { DataTypes } from "sequelize";

export const User = sequelize.define('user', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    name: {
        type: DataTypes.STRING(60),
        allowNull: false
    },
    username: {
        type: DataTypes.STRING(20),
        allowNull: false
    },
    password: {
        type: DataTypes.STRING(60),
        allowNull: false
    },
    permission: {
        type: DataTypes.STRING(20),
        allowNull: false
    }
}, {
    tableName: 'users',
})

