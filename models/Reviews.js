import { DataTypes } from 'sequelize';
import { sequelize } from '../databases/connectDb.js';
import { User } from './Users.js';
import { Publication } from './Publications.js'
import { Client } from './Clients.js';

export const Review = sequelize.define('review', {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  comment: {
    type: DataTypes.STRING(255),
  },
  stars: {
    type: DataTypes.INTEGER,
    allowNull: false
  },
  date: {
    type: DataTypes.DATE(),
    allowNull: false
  }
}, {
  tableName: "reviews"
});

Review.belongsTo(Publication, {
  foreignKey: {
    name: 'publication_id',
    allowNull: false
  },
  onDelete: 'RESTRICT',
  onUpdate: 'CASCADE'
})

Publication.hasMany(Review, {
  foreignKey: 'publication_id'
})

Review.belongsTo(User, {
  foreignKey: {
    name: 'user_id',
    allowNull: true
  },
  onDelete: 'RESTRICT',
  onUpdate: 'CASCADE'
})

User.hasMany(Review, {
  foreignKey: 'user_id'
})

Review.belongsTo(Client, {
  foreignKey: {
    name: 'client_id',
    allowNull: true
  },
  onDelete: 'RESTRICT',
  onUpdate: 'CASCADE'
})

Client.hasMany(Review, {
  foreignKey: 'client_id'
})