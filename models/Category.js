import { sequelize } from "../databases/connectDb.js";
import { DataTypes } from "sequelize";

export const Category = sequelize.define('category', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    name: {
        type: DataTypes.STRING(30),
        allowNull: false
    }
}, {
    tableName: 'categories',
})