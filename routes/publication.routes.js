import { Router } from 'express';
import { publicationAdminIndex, publicationCreate, publicationDelete, publicationIndex, publicationPesqByCategory, publicationPesqId, publicationPublicPesqId, publicationsPublics, publicationUpdate } from '../controllers/publicationController.js';
import { verifyLogin } from '../middlewares/verifyLogin.js';

const publicationRouter = Router();

publicationRouter
  .get('/', verifyLogin, publicationIndex)
  .get('/admin', verifyLogin, publicationAdminIndex)
  .get('/search_id/:id', verifyLogin, publicationPesqId)
  .get('/search_category/:category', verifyLogin, publicationPesqByCategory)
  .get('/publics', publicationsPublics)
  .get('/publics/search_id/:id', publicationPublicPesqId)
  .post('/create', verifyLogin, publicationCreate)
  .delete('/delete/:id', verifyLogin, publicationDelete)
  .put('/update/:id', verifyLogin, publicationUpdate)

export default publicationRouter;
