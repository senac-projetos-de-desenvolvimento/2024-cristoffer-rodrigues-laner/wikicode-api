import { Router } from 'express';
import { verifyLogin } from '../middlewares/verifyLogin.js';
import { categoryCreate, categoryDelete, categoryIndex, categoryPesqId, categoryUpdate } from '../controllers/categoryController.js';

const categoryRouter = Router();

categoryRouter
  .get('/', verifyLogin, categoryIndex)
  .get('/search_id/:id', verifyLogin, categoryPesqId)
  .post('/create', verifyLogin, categoryCreate)
  .put('/update/:id', verifyLogin, categoryUpdate)
  .delete('/delete/:id', verifyLogin, categoryDelete)

export default categoryRouter;
