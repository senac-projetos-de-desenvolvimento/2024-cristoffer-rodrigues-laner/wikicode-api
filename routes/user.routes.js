import { Router } from 'express';
import { userAdminIndex, userCreate, userDelete, userIndex, userIndexNames, userPesqId, userUpdate } from '../controllers/userController.js';
import { verifyLogin } from '../middlewares/verifyLogin.js';

const userRouter = Router();

userRouter
  .get('/', verifyLogin, userIndex)
  .get('/admin', verifyLogin, userAdminIndex)
  .get('/names', verifyLogin, userIndexNames)
  .get('/search_id/:id', verifyLogin, userPesqId)
  .post('/create', userCreate)
  .delete('/delete/:id', verifyLogin, userDelete)
  .put('/update/:id', verifyLogin, userUpdate)

export default userRouter;
