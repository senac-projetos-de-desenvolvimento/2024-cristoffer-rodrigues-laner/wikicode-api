import { Router } from 'express';
import { participantDelete } from '../controllers/participantController.js';

const participantsRouter = Router();

participantsRouter
  .delete('/delete/:id', participantDelete)

export default participantsRouter;
