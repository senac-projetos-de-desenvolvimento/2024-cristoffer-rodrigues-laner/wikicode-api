import { Router } from 'express';
import userRouter from './user.routes.js';
import publicationRouter from './publication.routes.js';
import loginRouter from './login.routes.js';
import participantsRouter from './participants.routes.js';
import categoryRouter from './category.routes.js';
import reviewRouter from './review.routes.js';
import clientRouter from './client.routes.js';

const router = Router();

router.use('/users', userRouter)
router.use('/publications', publicationRouter)
router.use('/login', loginRouter)
router.use('/participants', participantsRouter)
router.use('/categories', categoryRouter)
router.use('/reviews', reviewRouter)
router.use('/clients', clientRouter)

export default router;