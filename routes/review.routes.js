import { Router } from 'express';
import { verifyLogin } from '../middlewares/verifyLogin.js';
import { reviewCreate, reviewDestroy, reviewIndex, reviewPublication } from '../controllers/reviewController.js';
import { verifyClientLogin } from '../middlewares/verifyClientLogin.js';

const reviewRouter = Router();

reviewRouter
  .get('/', reviewIndex)
  .get('/search_id/:publication_id', reviewPublication)
  .post('/create', verifyLogin, reviewCreate)
  .post('/create/client', verifyClientLogin, reviewCreate)
  .delete('/delete/:id', verifyLogin, reviewDestroy)

export default reviewRouter;
