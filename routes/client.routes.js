import { Router } from 'express';
import { clientCreate, clientDelete, clientIndex } from '../controllers/clientController.js';
import { verifyLogin } from '../middlewares/verifyLogin.js';
import { loginClient } from '../controllers/loginClientController.js';

const clientRouter = Router();

clientRouter
  .get('/', verifyLogin, clientIndex)
  .post('/create', clientCreate)
  .delete('/delete/:id', verifyLogin, clientDelete)
  .post('/login', loginClient)

export default clientRouter;
