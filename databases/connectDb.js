import { Sequelize } from "sequelize";
import dotenv from "dotenv"
import pg from 'pg'

dotenv.config()

export const sequelize = new Sequelize(process.env.DATABASE_URL, {
    dialectOptions: { ssl: { require: true } },
    dialect: 'postgres',
    dialectModule: pg
});

// export const sequelize = new Sequelize(
//     process.env.DB_NAME,
//     process.env.DB_USER,
//     process.env.DB_PASSWORD,
//     {
//         url: process.env.DATABASE_URL,
//         dialect: process.env.DB_DIALECT
//     }
// )