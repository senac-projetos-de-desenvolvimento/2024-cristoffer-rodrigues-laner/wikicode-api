import jwt from 'jsonwebtoken'

import * as dotenv from 'dotenv'
dotenv.config()

export const verifyClientLogin = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(" ")[1]
    const decode = jwt.verify(token, process.env.JWT_SECRET_KEY)    
    req.client_logado_id = decode.client_logado_id
    req.client_logado_nome = decode.client_logado_nome
    req.client_logado_email = decode.client_logado_email
    next()
  } catch (error) {
    return res.status(401).send({ error, Erro: "Falha na Autenticação" })
  }
}